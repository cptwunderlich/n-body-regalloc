# n-body-regalloc

Inspecting slow-down in benchmark nofib/shootout/n-body.

Benchmark is ca. 60% slower for graph coloring allocator vs. linear scan allocator.
It does spill, but when using the modified GCRA ("chaitin"), no spills happen, but slow-down
is still there!

## Infos

Built with https://gitlab.haskell.org/cptwunderlich/ghc/-/commits/wip/bmaurer/ssa-regalloc

For _linear_:
> ~/Projekte/haskell/ghc/_graph/stage1/bin/ghc -g3 -O2 -fproc-alignment=64 -ddump-asm-liveness -ddump-cfg-weights -ddump-asm-regalloc -ddump-to-file -dppr-debug Main.hs

For _graph_:
> ~/Projekte/haskell/ghc/_graph/stage1/bin/ghc -g3 -O2 -fregs-graph -fproc-alignment=64 -ddump-asm-liveness -ddump-cfg-weights -ddump-asm-regalloc -ddump-to-file -dppr-debug Main.hs

For _chaitin_:
> ~/Projekte/haskell/ghc/_graph/stage1/bin/ghc -g3 -O2 -fregs-graph -fssa-transform -fregs-graph-chaitin -fproc-alignment=64 -ddump-asm-liveness -ddump-cfg-weights -ddump-asm-regalloc -ddump-to-file -dppr-debug Main.hs

## CFG

Folders contain CFG image. Block marked red is the offending loop (hopefully).
